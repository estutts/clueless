var Client = (function(window) 
{
	var socket		= null;
	var gameState	= null;
	
	var init = function()
	{
		socket = io.connect();
		
		attachDOMEventHandlers();
		attachSocketEventHandlers();
		
		socket.emit('join');
	};
	
	var attachDOMEventHandlers = function()
	{
		
	};
}(window));