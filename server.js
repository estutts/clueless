/*
 * Importing module dependencies
 */
var path 		= require('path'), 
    http		= require('http'), 
    express 		= require('express'),
    socket		= require('socket.io'),
    logger		= require('morgan'),
    cookieParser	= require('cookie-parser'),
    bodyParser  	= require('body-parser'),
    httpRoutes     	= require(path.join(__dirname, 'routes', 'http')),
    socketRoutes	= require(path.join(__dirname, 'routes', 'socket')),
    gameCollection 	= require(path.join(__dirname, 'lib', 'GameCollection'));

/*
 * Creating app and listeners
 */
var app 	= express(),
    server 	= http.createServer(app),
    io		= socket.listen(server),
    router 	= express.Router();

// Initializing a game container
var games = new gameCollection();

// Set port 
app.set('port', 3000);

// Tell the app where to get html and how to process it (jade)
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(router);

// Attach routes to share data
httpRoutes.attach(app, games);
socketRoutes.attach(io, games);

// Start the server (next step is http.js)
server.listen(app.get('port'), function()
{
   console.log("Socket.IO Server is lsitening on port " + app.get('port'));
});

