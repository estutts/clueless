//var _ = require('underscore');

function Game()
{
    this.num = 1;
};

Game.prototype.getNum = function()
{
    return this.num;
};

Game.prototype.inc = function()
{
    this.num = this.num + 1;
};

module.exports = Game;
