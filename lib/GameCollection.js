var Game = require('./Game'); //TODO: platform dependent

function GameCollection()
{
//    this.games = {};
      this.game = null;
};

GameCollection.prototype.add = function() 
{
    this.game = new Game();
    return this.game;
};

GameCollection.prototype.getGame = function()
{
    return this.game;
};

module.exports = GameCollection;
