var games = null;

/*
 * This function is called when someone connects to '/'.
 * Just need to render the home page.
 */
var home = function(req, res)
{
    res.render('home');
};

/*
 * This function is called when 
 */
var start = function(req, res)
{
    console.log('Starting game.');
    newGame = games.add();
    res.redirect('/game');
};

var game = function(req, res)
{
    // Need to add validation if someone goes here directly
    res.render('game');
};

//Catch-all function that redirects to home page.
var invalid = function(req, res)
{
	res.redirect('/');
};

// Set up attachments and html redirects to functions
exports.attach = function(app, gamesIn)
{
    games = gamesIn;
    app.get('/',	home);
    app.post('/start',   start);
    //app.get('/game',    game);
    app.all('*', invalid);
};
