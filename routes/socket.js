var IO = null;
var games = null;

exports.attach = function(io, gamesIn)
{
	IO = io;
	games = gamesIn;
	
	io.sockets.on('connection', function(socket)
	{
		console.log('Socket' + socket.id + ' connected');
	});
};